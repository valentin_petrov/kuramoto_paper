% This text is proprietary.
% It's a part of presentation made by myself.
% It may not used commercial.
% The noncommercial use such as private and study is free
% May 2007
% Author: Sascha Frank
% University Freiburg
% www.informatik.uni-freiburg.de/~frank/
%
%
\documentclass{beamer}
\setbeamertemplate{navigation symbols}{}

\usepackage{beamerthemeshadow}
\begin{document}
\title{Dynamics of finite-dimensional  Kuramoto model}
\author{V.N. Belykh, V.S. Petrov, G.V. Osipov}
\date{\today}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}\frametitle{Table of contents}\tableofcontents
\end{frame}

\section{Problem statement}
\begin{frame}\frametitle{Problem Statement}
\begin{block}{Well known Kuramoto model of phase oscillators}
\begin{equation}\label{eq1}
\dot{\phi}_i=\omega_i+\xi_i(t)+\frac{K}{N}\sum_{j=1}^{N}\sin(\phi_j-\phi_i) ,i=\overline{1,N}
\end{equation}
\end{block}


\begin{block}{The basic tool in studying the model \ref{eq1} is the "mean field" approach}
\begin{equation}\label{eq2}
re^{j(\psi-\phi_i)}=\frac{1}{N}\sum_{k=1}^{N}e^{j(\phi_k-\phi_i)},
\end{equation}
\begin{equation}\label{eq3}
\dot{\phi}_i=\omega_i+\xi_i(t)+Kr\sin(\psi-\phi_i) ,i=\overline{1,N}
\end{equation}
This change is valid when $r$ and $\psi$ in \ref{eq2} do not depend on $\phi_k$, and this is true for $N\xrightarrow[]{}\infty$.
\end{block}

\end{frame}
\begin{frame}\frametitle{Goal}
The main goal of this work is to present sufficient conditions for phase synchronization of the Kuramoto model for any finite number of phase oscillators.
\end{frame}

\section{Sufficient condition for phase synchronization}
\subsection{Preliminary transformations}
\begin{frame}\frametitle{Transformation}
\begin{block}{Phase differences}
\begin{equation}\label{eq6}
\Theta_j=\phi_j-\phi_1,\Delta_j=\omega_j-\omega_1+\xi_j-\xi_1 ,j=\overline{1,N}
\end{equation}
\end{block}

\begin{block}{``New order parameter''}
\begin{equation}\label{eq7}
\dot{\Theta}_i=\Delta_i-KR\sin \Theta_i ,i=\overline{1,N}
\end{equation}
\begin{equation}
R=\frac{1}{N}\sum_{j=1}^{N}\left(\frac{\cos(\Theta_i/2-\Theta_j)}{\cos(\Theta_i/2)}\right)
\end{equation}
\end{block}
\end{frame}
\begin{frame}\frametitle{Phase synchronization}
\begin{block}{Definition}
Phase synchronization of oscillators is defined as a steady state $\Theta^*_i(t), i=\overline{1,N}$ such, that
\begin{equation}\label{eq19}
|\Theta^*_i|\le\alpha, i=\overline{1,N},
\end{equation}
where $\alpha$ is a parameter of phases mismatch in \eqref{eq1}.
\end{block}
\begin{block}
Now we choose some value of $\alpha$ in \eqref{eq19}, $\alpha<\frac{\pi}{3}$, and obtain the bounds
\begin{equation}\label{eq21}
\cos\frac{3\alpha}{2}<R<\cos^{-1}\frac{\alpha}{2}
\end{equation}
\end{block}
\end{frame}
\subsection{Comparison systems}
\begin{frame}\frametitle{Comparison principle}

{\footnotesize
Given a system
\begin{equation}\label{eq23}
\dot{x}=F(x), x=(x_1,\dotsc,x_n), F=\left(F_1,\dotsc,F_n\right),
\end{equation}
and a compact $C=\{x\Vert|x_i|\leq\alpha, i=\overline{1,n}\}$ we introduce comparison system
\begin{equation}\label{eq24}
\begin{aligned}
\dot{x}_i=F^{+}_i(x_i), i=\overline{1,n}\\
\dot{x}_i=F^{-}_i(x_i), i=\overline{1,n}\\
|x_i|\leq\alpha
\end{aligned}
\end{equation}
where $F^{+}_i=\sup_{\substack{|x_j|\leq\alpha\\j\ne{i}}} F_i(x_1,\dotsc,x_n)$, $F^{-}_i=\inf_{\substack{|x_j|\leq\alpha\\j\ne{i}}} F_i(x_1,\dotsc,x_n)$.

}
\begin{exampleblock}{Theorem}
\textit{
Let each equation of comparison system \eqref{eq24} have unique stable equilibrium point $x^{+(-)}_i$ such. that $-\alpha<x^{-}_i\leq{x^{+}_i}<\alpha, i=\overline{1,n}$. Then, the compact $C$ is an absorbing domain, i.e. for any initial point $x^o\in{C}$, the trajectory of the system \eqref{eq23}, $x=\tilde{x}(x^o,t)$, lies in $C$, $\tilde{x}(x^o,t)\in{C}, t\in(o,\infty)$.}
\end{exampleblock}
\end{frame}
\begin{frame}\frametitle{Comparison equations for Kuramoto system}
\begin{block}{}
\begin{equation}\label{eq25}
\begin{aligned}
\dot{\Theta}_i=F^{\pm}_i(\Theta_i)=\Delta_i-KR^{\pm}\sin\Theta_i, i=\overline{1,N}\\
R^{+}= \begin{cases} \cos\frac{3\alpha}{2} &\mbox{for } \Theta_i\in[0,\alpha] \\
 cos^{-1}\frac{\alpha}{2} &\mbox{for }  \Theta_i\in[-\alpha,0) \end{cases}\\
R^{-}= \begin{cases} \cos^{-1}\frac{\alpha}{2} &\mbox{for } \Theta_i\in[0,\alpha] \\
 cos\frac{3\alpha}{2} &\mbox{for }  \Theta_i\in[-\alpha,0). \end{cases}\\
\end{aligned}
\end{equation}
\end{block}
\end{frame}
\subsection{Synchronization condition}
\begin{frame}\frametitle{Sufficient synchronization condition}

\begin{alertblock}{Theorem 2}
\textit{
The trajectory of the system \eqref{eq7} do not leave the compact $C$ for the parameter range}
\begin{equation}\label{eq26}
|\Delta_i|<K\cos\frac{3\alpha}{2}\sin\alpha
\end{equation}
\end{alertblock}

\end{frame}
\section{Numerical validation}
\begin{frame}\frametitle{Experiment 1}
\begin{figure}
\includegraphics[width=0.6\columnwidth]{../A_K.eps}
\caption{The dependency of synchronization phase locking angle $\alpha$ on $K$. Red-circle line - numerical result; blue curve - theoretical approximation. Insets represent the distribution of phases over unit circle for specified values of $K=0.1,1,3,8$.}
\end{figure}
\end{frame}

\begin{frame}\frametitle{Experiment 2}
\begin{figure}
\includegraphics[width=0.6\columnwidth]{../belykh_rst_K_delta.eps}
\caption{Numerically obtained synchronization regions for $\alpha=\pi/6,\pi/12$ as well as theoretical upper and lower bounds on critical $K$. }
\end{figure}
\end{frame}


\begin{frame}\frametitle{Experiment 3}
\begin{figure}
\includegraphics[width=0.6\columnwidth]{../K_alpha.eps}
\caption{The dependency of critical coupling $K$ on $\alpha$: magenta-squares - numerical; black-stars, green-circles - theoretical lower and upper bounds respectively. Red-triangles represent observed relative error, blue-circles - maximum possible error estimated theoretically.}
\end{figure}
\end{frame}
\section{Conclusions}
\begin{frame}\frametitle{Conclusions}
\begin{itemize}
\item We have studied the global synchronization mode of finite-demensional Kuramoto model.
\item For this purpose we presented a normal form obtained be eliminating the uncertainty in the original Kuramoto model.
\item We proposed a simple method of comparison which allowed to obtain explicit estimate of the phase synchronization range.
\item This estimate is valid both for constant and time varying frequency mismatch.
\item Detailed numerical simulations confirmed all theoretical results.
\end{itemize}

\end{frame}
\begin{frame}\frametitle{Thank you}

\begin{block}{}
\centering{Q\&A}
\end{block}
\end{frame}

\end{document}
