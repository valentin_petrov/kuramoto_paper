#!/bin/bash

for f in 'freqs_K_0.36_delta_2' 'freqs_K_2_delta_0.19' 'freqs_K_2_delta_2'  'freqs_K_8_delta_0.19' ; do
    gnuplot -e "in_file='"$f".dat'" -e "out_file='"$f".eps'" plot_freqs.gpt
done
