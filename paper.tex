\documentclass[a4paper]{article}% APS journal style
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{hyperref}% add hypertext capabilities
\usepackage{color}
\usepackage{authblk}
\usepackage{amsmath}
\usepackage{amsthm}


\newtheorem{theorem}{Theorem}
\newtheorem{assumption}{Assumption}
\newtheorem{corollary}{Corollary}

%--------------------------------------------------------
\begin{document}
\title{Dynamics of finite-dimensional  Kuramoto model: Global and cluster synchronization}

\author[1]{V.N. Belykh}
\author[1]{V.S. Petrov}
\author[1]{G.V. Osipov}
\affil[1]{Department of Control Theory, Nizhny Novgorod University, 23,
	Gagarin Ave., 603950, Nizhny Novgorod, Russia}

\date{\today}


\maketitle

\begin{abstract}
Synchronization phenomena in networks of globally coupled non-identical oscillators is one of the key problems in nonlinear dynamics over the years. The main model used within this framework is Kuramoto model.  This model shows three main types of behavior: global synchronization, cluster synchronization including chimera states and totally incoherent bahavior. We present new sufficient conditions of phase synchronization and conditions of asynchronous mode in finite-size Kuramoto model. In order to find these conditions for constant and time varying frequency mismatch we propose a simple method of comparison which allowed to obtain explicit estimate of the phase synchronization range. Theoretical results  are supported by numerical simulations.

Keywords: phase oscillators, Kuramoto model, global synchronization, existence
and stability conditions, asynchronous mode.

\end{abstract}

\section{Introduction}

	The formation of collective behavior in large ensembles or
networks  of coupled non-identical oscillatory elements is one of the oldest
problem in the study of dynamical systems. Nevertheless, it is an
actually  challenging field for a theoretical understanding as
well as for applications in various disciplines, ranging from
physics, chemistry, earth sciences via biology and neuroscience to
engineering, business and social sciences. Due to the large number
of effective degrees of freedom in spatially extended systems, a
rich variety of spatiotemporal regimes is  observed. Three main
types of collective behavior are distinguished: (i) a fully
incoherent state or highly developed spatiotemporal disorder; (ii)
partially coherent states, where some of the participants in the
network behave in some common rhythm; (iii) a fully coherent state
or a regime of globally synchronized elements. The basic
phenomenon of these structure formations is synchronization, i.e.
regime of coherent activity, which is universal in many dynamical
systems and can be understood from the analysis of common models
of oscillatory networks.



Cooperative phenomena in ensembles of globally (mean-field)
coupled  phase equations were studied first by Winfree \cite{Winfree_1}.
He  mathematically reduced the problem of mutual synchronization to that
of a collective behavior in an ensemble of coupled {\it phase oscillators}.
He showed that such oscillator ensembles  could demonstrate a temporal
analog of a thermodynamic phase transition: with the increase
of the coupling a group of oscillators suddenly becomes synchronized.
By using perturbation methods of averaging, Kuramoto \cite{Kuramoto,Kuramoto_1}
obtained a model of weakly {\it globally} coupled, nearly identical
limit-cycle oscillators. The governing system of this model are first-order
dynamical equations  with sinusoidal coupling function. This paradigmatic
model appears in many applications in science and engineering.
Examples are cells in the heart  \cite{Michaels},  Hodgkin-Huxley neurons \cite{Brown},
central pattern generator for animal locomotion \cite{Kopell}, coupled Josephson
junctions \cite{Wiesenfeldt},  rhythmic applause \cite{Neda}, pedestrian crowd
synchrony on London's Millennium bridge \cite{Strogatz}, semiconductor laser
arrays \cite{Kozyreff}, microwave oscillator arrays \cite{York} etc.
For other examples,  see \cite{Doerfler}.

Most results for the Kuramoto model are obtained in the case of infinite
dimension (for review see, \cite{Doerfler, Acebron, Gupta}). In finite-dimensional
case various conditions of the synchronization band have been proposed \cite{Doerfler, Aeyels, Mirollo, Verwoed, Jadbabaie}. 
In this paper, we present new sufficient conditions of phase synchronization and conditions of asynchronized mode in finite-size Kuramoto model.

\section{Theoretical estimates}
The well known Kuramoto model \cite{Kuramoto_1} of phase oscillators has the form:
\begin{equation}\label{eq1}
\dot{\phi}_i=\omega_i+\xi_i(t)+\frac{K}{N}\sum_{j=1}^{N}\sin(\phi_j-\phi_i) ,i=\overline{1,N},
\end{equation}

here $\phi_i$ - phases, $\omega_i$ - natural frequencies, $\xi_i(t)$ - external noise, $K$ - coupling, $N$ - number of oscillators.

The basic tool in studying the model \eqref{eq1} is the "mean field" approach. That is the substitution:
\begin{equation}\label{eq2}
re^{j(\psi-\phi_i)}=\frac{1}{N}\sum_{k=1}^{N}e^{j(\phi_k-\phi_i)},
\end{equation}
and the right part in \eqref{eq1} is changed such that
\begin{equation}\label{eq3}
\dot{\phi}_i=\omega_i+\xi_i(t)+Kr\sin(\psi-\phi_i) ,i=\overline{1,N}
\end{equation}


This change is valid when $r$ and $\psi$ in \eqref{eq2} do not depend on $\phi_k$, and this is true for $N\xrightarrow[]{}\infty$. The latter constitutes the "mean field" in "all-to-all" configuration of phase oscillators \eqref{eq1}.


For the finite number $N$ the order parameters $r$ and $\phi$ can be written in the form
\begin{equation}\label{eq4}
\begin{aligned}
r=\sqrt{\left(\frac{1}{N}\sum_{k=1}^{N}\sin{\phi_k}\right)^2+\left(\frac{1}{N}\sum_{k=1}^{N}\cos{\phi_k}\right)^2}\leq1\\
\psi=\arctan{\frac{\sum_{k=1}^{N}\sin{\phi_k}}{\sum_{k=1}^{N}\cos{\phi_k}}}
\end{aligned}
\end{equation}
and straightforward conclusions about synchronization for finite number $N$ via \eqref{eq4} cannot be done.


The main goal of this paper is to present sufficient conditions for phase synchronization in the Kuramoto model for any finite number of phase oscillators.

\subsection{Properties of Kuramoto model}
\begin{enumerate}
\item[1.]
Consider Kuramoto equations for homogenous perturbation $\xi_i(t) \equiv \xi(t)$,
\begin{equation}\label{eq5}
\dot{\phi}_i=\omega_i+\xi(t)+\frac{1}{N}\sum_{j=1}^{N}\sin(\phi_j-\phi_i) ,i=\overline{1,N}
\end{equation}
where $\xi(t)$ is an arbitrary continuous function.
The variables change $\phi_i=\tilde{\phi}_i+\int{\xi(t) dt}$ reduces the system \eqref{eq5} to the original unperturbed system. Hence, Kuramoto model admits
\begin{itemize}
\item
well known transition to rotation mode when $\xi(t)=\Omega=const$, that is why one consider
$\sum^{N}_{i=1}\omega_i=0$
\item
simultaneous escape of all phases, when, for instance, $\xi(t)=e^t$
\item
effects of additive common noise are equivalent to additive perturbation of the stationary process of the unperturbed system.
\end{itemize}
\item[2.]
The unperturbed Kuramoto system has the invariant foliation $\sum^{N}_{i=1}\phi_i=const$, as far as $\sum^{N}_{i=1}\dot{\phi}_i=\sum^{N}_{i=1}\omega_i$. This implies that Kuramoto model is an excessive system of ODE, and cannot have any asymptotically stable states: the coordinate "$u$" along the vector $(1,1,\dots,1)$ satisfies equation $\dot{u}=0$.
\end{enumerate}
\subsection{Phase differences equations}
We rewrite the system \eqref{eq1} in order to exclude one equation. We choose the first oscillator as a leading one and introduce new variables and new parameters.
\begin{equation}\label{eq6}
\Theta_j=\phi_j-\phi_1,\Delta_j=\omega_j-\omega_1+\xi_j-\xi_1 ,j=\overline{1,N}
\end{equation}
where $\Theta_j$ is the phase difference and $\Delta_j$ is the frequency mismatch of $j$'s and leading oscillators.


\textbf{Proposition.} The system \eqref{eq1} can be rewritten in the form.

\begin{equation}\label{eq7}
\dot{\Theta}_i=\Delta_i-KR\sin \Theta_i ,i=\overline{1,N}
\end{equation}

where $R=\frac{1}{N}\sum_{j=1}^{N}\left(\frac{\cos(\Theta_i/2-\Theta_j)}{\cos(\Theta_i/2)}\right)$.


Note, that the trivial first equation $\dot{\Theta}_1=0,\Theta_1=0,\Delta_1=0$ reduces dimension of the system due to the invariant foliation existence, 
and the function $R$ can be referred to as a new "order parameter".


Indeed, introducing phase differences and frequency mismatch \eqref{eq6} we rewrite the system in the form:

\begin{equation}\label{eq8}
\begin{aligned}
\dot{\Theta}_i = \omega _i+\frac{K}{N}(\sin(\Theta_1-\Theta_i)+\sin(\Theta_2-\Theta_i)+\cdots+\sin(\Theta_n-\Theta_i))\\
\dot{\Theta}_1 = \omega _1+\frac{K}{N}(\sin\Theta_1+\sin\Theta_2+\cdots+\sin\Theta_n),
\end{aligned}
\end{equation}
from which we obtain equations for the new variables

\begin{equation}\label{eq9}
\dot{\Theta}_i=\Delta _i-\frac{K}{N}\sum\limits_{j=1}^N \left(\sin \Theta_j - \sin(\Theta_j - \Theta_i)\right).
\end{equation}

Using the formula $\sin \Theta_j - \sin(\Theta_j - \Theta_i) = 2 \sin \frac{\Theta_i}{2}\cos(\frac{\Theta_i}{2} - \Theta_j)$ we come to the final form of the system

\begin{equation}\label{eq10}
\dot{\Theta}_i=\Delta _i-K\left[\frac{2}{N}\sum_{j=1}^{N}\cos\left(\frac{\Theta_i}{2}-\Theta_j\right)\right]\sin \frac{\Theta_i}{2},
\end{equation}
which is equivalent to the system (\ref{eq7}).

\section{Dynamics of the Kuramoto system}
\subsection{Identical oscillators}
In this trivial case the frequency differences $\Delta_i=0$, the equation \eqref{eq7} takes the form
\begin{equation}\label{eq14}
\dot{\Theta}_i=-K\frac{1}{N}\sum_{j=1}^{N}\left(\frac{\cos(\Theta_i/2-\Theta_j)}{\cos(\Theta_i/2)}\right)\sin \Theta_i ,i=\overline{1,N}
\end{equation}


Zero equilibrium point $O$ ($\Theta_i=0, i=\overline{1,N}$) is exponentially stable as far as variational equation is $\dot{u}_i=-Ku_i$. The basin of equilibrium $O$ is
\begin{equation}\label{eq15}
|\Theta_i|<\frac{\pi}{3} ,i=\overline{1,N}
\end{equation}


It follows from Lyapunov function
\begin{equation}\label{eq16}
V=\sum_{i=1}^{N}(1-\cos\Theta_i),
\end{equation}
which derivative with respect to \eqref{eq14} is
$$\dot{V}=-K\frac{1}{N}\sum_{j=1}^{N}\left(\frac{\cos(\Theta_i/2-\Theta_j)}{\cos(\Theta_i/2)}\right)\sin^2 \Theta_i < 0 for |\Theta_i|<\frac{\pi}{3} , \Theta_i\ne0,i=\overline{1,N}.$$

\subsection{Equal frequency differences $\Delta_i=\Delta=const$ (all are similarly against leader)}


Equation \eqref{eq7} takes the form
\begin{equation}\label{eq17}
\dot{\Theta}_i=\Delta-KR\sin\Theta_i
\end{equation}


This system has 1D invariant manifold $J=\{\Theta_i=\Theta, i=\overline{1,N}\}$. The dynamics in J is due to the equality $R\vert_{J}=1$
\begin{equation}\label{eq18}
\dot{\Theta}=\Delta-K\sin\Theta
\end{equation}
and $|\Delta|<K$ is the condition of stable synchrony in $J$ ($\Theta^*=\arcsin\frac{\Delta}{K}$). The transversal to $J$ stability one can derive using variational equations.

\section{Sufficient condition of phase synchronization of Kuramoto oscillators}
Consider for simplicity the case of different constant values of frequency mismatches in (\ref{eq7}).

Phase synchronization of oscillators is defined as a steady state $\Theta^*_i(t), i=\overline{1,N}$ such, that
\begin{equation}\label{eq19}
|\Theta^*_i|\le\alpha, i=\overline{1,N},
\end{equation}
where $\alpha$ is a parameter of phases mismatch in \eqref{eq1}.


The function $R$ in \eqref{eq7} satisfies the condition
\begin{equation}\label{eq20}
\frac{1}{N}\sum_{j=1}^{N}\left(\frac{\cos(\Theta_i/2-\Theta_j)}{\cos(\Theta_i/2)}\right)>0 \ \ \ for \ \ \ |\Theta_i|<\frac{\pi}{3} ,i=\overline{1,N}
\end{equation}


Now we choose some value of $\alpha$ in \eqref{eq19}, $\alpha<\frac{\pi}{3}$, and obtain the bounds
\begin{equation}\label{eq21}
\cos\frac{3\alpha}{2}\cos^{-1}\frac{\Theta_i}{2}<R<\cos^{-1}\frac{\Theta_i}{2}
\end{equation}


Using \eqref{eq21} we introduce two auxiliary systems
\begin{equation}\label{eq22}
\begin{aligned}
\dot{\Theta}_i=\Delta_i-2K\sin\frac{\Theta_i}{2}, i=\overline{1,N}\\
\dot{\Theta}_i=\Delta_i-2K\cos\frac{3\alpha}{2}\sin\frac{\Theta_i}{2}, i=\overline{1,N}
\end{aligned}
\end{equation}
defined in the compact $C=\{\Theta\Vert|\Theta_i|<\alpha, i=\overline{1,N}\}$.

\subsection{Comparison principle}
In order to obtain sufficient conditions of phase synchronization we use comparison principle, which is formulated as follows. 
Given a system
\begin{equation}\label{eq23}
\dot{x}=F(x), \\
x=(x_1,\dotsc,x_n), F=\left(F_1,\dotsc,F_n\right),
\end{equation}
and a compact $C=\{x | \ |x_i|\leq\alpha, i=\overline{1,n}\}$. We introduce comparison systems
\begin{equation}\label{eq24}
\begin{aligned}
\dot{x}_i=F^{+}_i(x_i), i=\overline{1,n}\\
\dot{x}_i=F^{-}_i(x_i), i=\overline{1,n}\\
|x_i|\leq\alpha
\end{aligned}
\end{equation}
where $F^{+}_i=\sup_{\substack{|x_j|\leq\alpha\\j\ne{i}}} F_i(x_1,\dotsc,x_n)$, $F^{-}_i=\inf_{\substack{|x_j|\leq\alpha\\j\ne{i}}} F_i(x_1,\dotsc,x_n)$.
\begin{assumption}\label{assump1}
Assume that $F^{\pm}_i(x_i)$ are continuous functions such that each of them has a unique zero at the interval $[-\alpha, \alpha]$, $x_i=\beta^{\pm}_i\in [-\alpha, \alpha]$,
and $F^{\pm}_i(x_i)\cdot(x_i-\beta^{\pm}_i)<0$, $x_i \in [-\alpha, \alpha]$, $x_i\neq \beta^{\pm}_i$.
\end{assumption}

Due to these conditions the points $\beta^{\pm}_i$ are stable equilibrium points of the comparison system \eqref{eq24}.
%figure
As $F^{+}_i\geq F^{-}_i$, $x_i \in [-\alpha, \alpha]$, so $\beta^{-}_i \leq \beta^{+}_i$, and we introduce a compact $C_{\beta}=\{x | \beta^{-}_i \leq x_i \leq \beta^{+}_i, i=1,\ldots, n\}$,
$C_{\beta}\subset C$.

\begin{theorem}\label{thm1}
Let the assumption \ref{assump1} holds. Then the system \eqref{eq1} has an attractor A lying in the compact $C_{\beta}$, and the compact $C$ lies in the basin of the attractor A, $A\subset C_{\beta}\subset C$.
\end{theorem}

\begin{proof}
Consider directing Lyapunov function
\begin{equation}
V=\sum_{i=1}^{n}\left| x_i - \frac{\beta^+_i-\beta^-_i}{2}\right|
\end{equation}

in the domain $C\backslash C_{\beta}$.
It's derivative with respect to the system (\ref{eq23}) is
\begin{equation}
\dot{V} = \sum \Phi_i
\end{equation}
where
\begin{equation}
\Phi_i \ = \begin{cases}
F_i, &\mbox{if } \ x_i \in [\beta^+_i,\alpha],\\
-F_i, &\mbox{if } \ x_i \in [-\alpha, \beta^-_i],
\end{cases}
\end{equation}
Since the inequalities
\begin{equation}
F_i \leq F^+_i<0, \ x_i \in [\beta^+_i, \alpha],\\
-F_i<-F_i^-<0, \ x_i \in [-\alpha, \beta^-_i]
\end{equation}
hold, $\dot{V}<0$ for $x \in C\backslash C_{\beta}$.
Hence any trajectory of the system \eqref{eq23} starting at the initial point $x_0 \in C\backslash C_{\beta}$ enters the compact $C_{\beta}$, and $C_{\beta}$ contains a limiting set - attractor A.
\end{proof}

\begin{corollary}
There exists at least one equilibrium point of the system \eqref{eq23} in the compact $C_{\beta}$. This statement follows from Brouwer fixed point theorem for the map
$\eta: x\rightarrow (x+\tau F(x))$, where $\tau<0$ is small enough, since $\eta{C_{\beta}} \subset C_{\beta}$.
\end{corollary}

\begin{corollary}
The theorem is true in the case when the system \eqref{eq23} is nonautonomous. Here the functions in \eqref{eq24} should be replaced with $\tilde{F^+_i}=\sup_{t \in R`} F^+_i$,
$\tilde{F^-_i}=\inf_{t \in R`} F^-_i$ and a bound set $A_t \in C_{\beta}\times R`$ plays the role of the attractor A.
\end{corollary}


Using auxiliary systems \eqref{eq22} we compose the comparison systems
\begin{equation}\label{eq25}
\begin{aligned}
\dot{\Theta}_i=F^{\pm}_i(\Theta_i)=\Delta_i-KR^{\pm}\sin\frac{\Theta_i}{2}, i=\overline{1,N}\\
R^{+}= \begin{cases} 2 &\mbox{for } \Theta_i\in[-\alpha,0) \\
 2cos\frac{3\alpha}{2} &\mbox{for }  \Theta_i\in[0,\alpha] \end{cases}\\
R^{-}= \begin{cases} 2\cos\frac{3\alpha}{2} &\mbox{for } \Theta_i\in[-\alpha,0) \\
 2 &\mbox{for }  \Theta_i\in[0,\alpha]. \end{cases}\\
\end{aligned}
\end{equation}

From comparison principle we derive the following sufficient condition of Kuramoto oscillators' phase synchronization.

\begin{theorem}\label{thm2}
In the parameter range
\begin{equation}\label{eq26}
|\Delta_i| < 2K sin\alpha(cos\alpha-\frac{1}{2})
\end{equation}
the system \eqref{eq1} has an attractor $A \subset C$ corresponding to the phase synchronization with the phase mismatch equal to $\alpha$.
\end{theorem}

\begin{proof}
The comparison system \eqref{eq25} have the stable equilibrium points
\begin{equation}
\beta^{-(+)}_i=2arcsin\Delta_i(2K)^{-1},\\
\beta^{+(-)}_i=2arcsin\Delta_i(2Kcos\frac{3\alpha}{2})^{-1}
\end{equation}
for $\Delta_i>0$ ($\Delta_i<0$, respectively), $i=1,\ldots,N$. Then the range of the frequency mismatch $\Delta_i$ is defined by the condition $-\alpha<\beta^{-}_i<\beta^{+}_i<\alpha$,
i.e. $C_{\beta} \subset C$. From the critical values $\beta^-_i=-\alpha$, $\beta^+_i=\alpha$ we obtaining the band $|\Delta_i|<2Kcos\frac{3\alpha}{2}sin\frac{\alpha}{2}$,
equivalent to \eqref{eq26}.
\end{proof}

It is easy to verify, that the time dependence of mismatch $\Delta_i$ does not affect the synchronization range when $\Delta_i(t)$ satisfies \eqref{eq26} for any $t > 0$, i.e. the synchronization mode is insensitive to bounded perturbations.

\section{Condition of asynchronous mode of Kuramoto oscillators}
Asynchrony of oscillators implies the mean increase(decrease) of all phase differences $\Theta_i$. From the system \eqref{eq10} we obtain a "rough" sufficient condition of such mode.
\begin{theorem}\label{thm3}
In the parameter range
$$|\Delta_i|>2K$$
all trajectories of the system \eqref{eq10} rotate, and all $N$ oscillators  are asynchronous.
\end{theorem}

Indeed, in this case $\dot{\Theta}_i>0 (\dot{\Theta}_i<0), i=\overline{1,N}$ due to the equations \eqref{eq10}.


\section{Numerical validation}
In this section we provide the results of numerical simulation of the Kuramoto ensemble and compare them against theoretical estimates obtained previously. Throughout the simulations were considered various values of the number of oscillators $N$ and two kinds of individual frequencies distributions. The first distribution is linear of the kind $\omega_i = \omega_0 + i*\Delta/N$, $i=\overline{1,N}$. The second one is random uniform distribution in the interval $[\omega_0,\omega_0+\Delta]$.


The condition \eqref{eq26} gives the relation between the synchronization phase angle $\alpha$, coupling $K$ and frequency mismatch $\Delta$. So, in the first numerical experiment we compare the dependency of $\alpha$ on $K$ for fixed $\omega_0=1,\Delta=1$. The size of the ensemble $N=100$. The results are shown in Fig~\ref{fig::alpha_k}. The red curve with circles is obtained numerically while solid blue is the theoretical approximation \eqref{eq26}. First of all, note that for small $K$ there is no synchronization in the system. Hence even numerical curve breaks here. The corresponding distribution of oscillators over the phase circle is shown in the inset for $K=0.1$. The phases distributed almost uniformly over the circle although some clusters already start to establish since the coupling is non zero. Next, for large values of $K$ the locking angle goes down as expected and the almost exact correspondence of simulation and theory is observed. However, if we start decreasing $K$ from that point then as $\alpha$ increases the divergence of theory and experiment happens. This is due to the fact the all the estimations made are valid for $\alpha < \pi/3$ and the condition \eqref{eq26} has a singularity at this point (the right hand side turns zero which demands infinitely large $K$ in order to hold the inequality).
\begin{figure}[ht]
\centering
\includegraphics[width=0.9\columnwidth]{A_K.eps}
\caption{The dependency of synchronization phase locking angle $\alpha$ on $K$. Red-circle line - numerical result; blue curve - theoretical approximation. Insets represent the distribution of phases over unit circle for specified values of $K=0.1,1,3,8$.}
\label{fig::alpha_k}
\end{figure}


Now, let us compare the synchronization region provided by \eqref{eq26} with the exact one obtained numerically. We consider $N=10,100$ and for each experiment we fix $\alpha$ at some value: $\pi/6,\pi/12$. Both linear and random distributions are tested. In each experiment for fixed $\alpha$ we very $\Delta$ ($\omega_0=1$) and for all of its values we find the $K$ sufficient to provide synchronization in the ensemble within the angle $\alpha$. The results are show in Fig.~\ref{fig::synchr_region}. The two red line with start markers there represent synchronization boundary obtained numerically for $\alpha=\pi/6,\pi/12$ and linear frequencies distribution. The boundaries are linear and the one for $\alpha=\pi/12$ has larger $K$. The curves with circle and rectangle markers correspond to the upper and lower theoretical margins respectively (blue for $\alpha=\pi/6$ and red for $\alpha=\pi/12$). One can observe that numerical boundary is always inside the two theoretical margins. Moreover, the gap is smaller for larger $\alpha$ and smaller $\Delta$. Overall correspondence between the numerical data and theory is quite good. There are two more data plots in Fig.\ref{fig::synchr_region} with up- and down-triangle markers (no line). These represent the experiments with random delta distribution for $N=10,\alpha=\pi/12$ and $N=100,\alpha=\pi/6$. It is clearly seen that they exactly match red lines which means there is no dependency at all on $N$ an distribution type. What matters is $\alpha$. Theoretical approximation \eqref{eq26} does not have any dependency on $N$ and frequency distribution as well.

\begin{figure}[ht]
\centering
\includegraphics[width=0.9\columnwidth]{belykh_rst_K_delta.eps}
\caption{Numerically obtained synchronization regions for $\alpha=\pi/6,\pi/12$ as well as theoretical upper and lower bounds on critical $K$. See text for details.}
\label{fig::synchr_region}
\end{figure}


Finally, we find how coupling strength $K$ depends on $\alpha$. The numerical result is shown in Fig.\ref{fig::k_alpha} with magenta curve with square markers. The corresponding lower and upper theoretical limits on $K$ are plotted as black-star and green-circle curves respectively. One can see that the numerical result and both theoretical limits tend to meet each other for low alphas while the difference becomes more and more significant when $\alpha$ approaches $\pi/3$. The relative observed error between the theoretical synchronization limit (which is its upper bound) and experimental critical $K$ is shown with red-triangle line. The error is almost equal 1 for small alpha (which implies exactness of the theoretical approximation there) and grows for larger alpha. We can provide a theoretical estimate on this relative error: $e=\cos(\alpha/2)/\cos(3\alpha/2)$ (this follows from the right hand side of the comparison systems). It is shown with blue-circle curve in Fig.\ref{fig::k_alpha}. Firstly, it is always above observed error which means that the estimate is correct. Secondly, it is remarkable that this relative error depend on $\alpha$ only and does not depend even on $\Delta$. This means that the theoretical approximation \eqref{eq26} can be used for any frequency distribution with the same relative error.
\begin{figure}[ht]
\centering
\includegraphics[width=0.9\columnwidth]{K_alpha.eps}
\caption{The dependency of critical coupling $K$ on $\alpha$: magenta-squares - numerical; black-stars, green-circles - theoretical lower and upper bounds respectively. Red-triangles represent observed relative error, blue-circles - maximum possible error estimated theoretically. See text for details.}
\label{fig::k_alpha}
\end{figure}


This result concludes the numerical validation section. We showed here that the main result of the paper, which is the theoretical condition \eqref{eq26} does holds for different $N,\Delta,\alpha$. It approximates reality almost exactly for small $\alpha$ while for larger values it has a relative error that depends on $\alpha$ only. We also gave an estimate on this error.

\section{Sufficient conditions of the chimera states}
We introduce two parts of oscillators: first $N_s$, and second $N_a$, $N=N_s+N_a$, and the parameter $\mu=\frac{N_a}{N}$ indicating a fraction of the second
oscillators in the ensemble. Let us rewrite the equations \eqref{eq10} for each part of oscillators in the following form

\begin{equation}\label{eq27}
\begin{aligned}
\dot{\Theta}_i=\tilde{\Delta}_i-2K(1-\mu)\left[ \frac{1}{N_s}\sum_{j=1}^{N_s}cos(\frac{\Theta_i}{2}-\Theta_j) \right] sin\frac{\Theta_i}{2}, \\
\tilde{\Delta}_i=\Delta_i-2K\mu\left[\frac{1}{N_a}\sum_{k=1}^{N_a}\cos(\frac{\Theta_i}{2}-\Theta_{N_s+k})\right]sin\frac{\Theta_i}{2}, i=1,\ldots,N_s
\end{aligned}
\end{equation}
and
\begin{equation}\label{eq28}
\dot{\Theta}_i=\Delta_i-2K\left[ \frac{1}{N}\sum_{j=1}^{N}\cos(\frac{\Theta_i}{2}-\Theta_j)\right]\sin\frac{\Theta_i}{2}, \ i=N_s+1,\ldots,N.
\end{equation}

Due to the theorem \ref{thm3} the second part of oscillators is asynchronous with respect to the leading oscillator, independently on dynamics of the first
part oscillators, under the condition

\begin{equation}\label{eq29}
|\Delta_i|>2K , i=\overline{N_s+1,N}
\end{equation}

Now we obtain a condition for the first part of oscillators to be synchronous with respect to the leading oscillator. In order to use the comparison
principle for subsystem \eqref{eq27} we introduce the bounds of the terms $\tilde{\Delta}$.

\begin{equation}\label{eq30}
\Delta_i-2K\mu<\tilde{\Delta}_i<\Delta_i+2K\mu, \ i=1,\ldots,N_s.
\end{equation}

Note that these bounds are compatible with \eqref{eq29}.

\begin{theorem}\label{thm4}
Let both the following conditions

\begin{equation}\label{eq31}
|\Delta_i|<2K((1-\mu)cos\frac{3\alpha}{2}sin\frac{\alpha}{2}-\mu), \ i=1,\ldots,N_s.
\end{equation}

for the first part of oscillators, and the conditions \eqref{eq29} for the second part of oscillators hold. Then the system \eqref{eq1} has a chimera state such that first $N_s$
oscillators (including the leading one) are phase synchronized, and all the rest $N_a$ oscillators are asynchronous to the first $N_s$ oscillators.
\end{theorem}

\begin{proof}
Similarly to the theorem \ref{thm2} we conclude that if the bounds \eqref{eq30} are located within the range \eqref{eq26} for $i=1,\ldots,N_s$, i.e. the next inequalities hold

\begin{equation}\label{eq32}
-2Ksin\frac{\alpha}{2}cos\frac{3\alpha}{2}<\Delta_i-2K\mu,\\
2Ksin\frac{\alpha}{2}cos\frac{3\alpha}{2}>\Delta_i+2K\mu.
\end{equation}
the first part of oscillators is in phase synchronous state with the leading oscillator. The conditions \eqref{eq31}  follows from \eqref{eq32}.
\end{proof}

\subsection{Numerical proof}
In this section we show the correctness of Theorem \ref{thm4} by direct simulation of an ensemble of $N=100$ oscillators. We chose $\mu=0.1$, i.e. there should be 90 oscillators comprising synchronous cluster as well as 10 asynchronously rotating units alltogether forming a chimera state. The frequencies of the ensemble are chosen in the following way: (i) the first 90 frequencies are drawn from uniform random distribution with $\omega_i \in [1:1+\Delta_1]$; (ii) the frequencies of the last 10 oscillators are $\omega_i = f_i+\xi$, where $f_i$ are chosen randomly again from $[1:1+\Delta_2]$ and $\xi$ is the frequency gap between the first 90 and the last 10 oscillators. With $K=4, \Delta_1=1, \Delta_2=2,\xi=8$ the conditions \eqref{eq29} and \eqref{eq31} are met and one observes the chimera state in the system according to the theory. Fig.\ref{chimera} illustrates the chimera state for this parameter set. Firstly, we calculated the phase differences $\delta\varphi_i(t) = \varphi_i(t) - \varphi_0(t)$ and then those values are vizualized at the time moments $t_n = T_0*n$ where $T_o$ - is the rotation period of the first oscillator. This strobe plot is shown in Fig.\ref{chimera} using blue circles and left axes. One can see that while all $\delta\varphi_i$ for $i < 90$ lie inside fixed angle implying phase synchronization  of those 90 units, other 10 values are occupy the whole interval $[0:2\pi]$ which means those oscillators rotate with respect to first oscillator. The individual frequencies of the oscillators are shown in the same figure with red filled circles using right axes. Those frequencies prove as well the existance of the chimera state here.
\begin{figure}[ht]
\centering
\includegraphics[width=0.9\columnwidth]{data/chimera.eps}
\caption{Chimera state in the Kuramoto system for $N=100, \mu=0.1, K=4, \Delta_1=1, \Delta_2=2,\xi=8$. Blue empty circles (left axes) represent strobe plot of the chimera state and red filled circles correspond to the oscillators frequencies (see text for details).}
\label{chimera}
\end{figure}


However, in the same system other regimes may be observed for different values of $\Delta_2$ and $K$. For example increasing $K$ may lead to global synchronization in the system while decreasing $\Delta_2$ may result in the synchrony inside last 10 oscillators thus providing 2-cluster synchronous regime. So, the diagramm of the dynamical regimes on the parameter plane $\Delta_2,K$ was calculated in order to analyze the possible transitions between the different states. The diagramm is shown in Fig.\ref{fig::chimera_diag}. Four main dynamical  regimes are observed: GS - global synchronization, AS - global asynchrony; 2C - two cluster regime; CH - chimera state. The frequency distributions for 4 different $(\Delta_2,K)$ points (denoted in the plot with (a), (b), (c), (d)) are shown in Fig.\ref{fig::chimera_diag}(a,b,c,d) respectively. One can conclude from the diagramm that there are 2 different scenarios for transition to chimera state. The first one is realized when $\Delta_2$ is fixed and large some critical value and $K$ is increased. In this case the transition has the form AS$\rightarrow$CH. Further increase of $K$ in this case gives: CH$\rightarrow$2C$\rightarrow$GS. Alternative scenario for chimera state onset is observed when $K$ is fixed and $\Delta_2$ is being increased. Depending on $K$ two transitions observed: GS$\rightarrow$2C$\rightarrow$CH and 2C$\rightarrow$CH. In both cases chimera state sets in after 2 cluster synchronous regime. Interstingly that the region occupied by the AS regime does not depend on $\Delta_2$. In other words we can not observe the syncrhonization in last 10 oscillators while the first 90 are not syncrhonized (if we could then this would be a chimera with 10 synched and 90 rotating units). The unexpected fact that this is true even for very small $\Delta_2$ including $\Delta_2=0$, i.e. when last 10 oscillators are identical. This is because for such small $K$ the 90 oscillators desynchronize the 10 left even when the latter are identical.

\begin{figure}[ht]
\centering
(a)\includegraphics[width=0.9\textwidth]{data/K_delta2_chimera_diagramm.eps}\\
(b)\includegraphics[width=0.4\textwidth,height=0.3\textwidth]{data/freqs_K_0.36_delta_2.eps}
(c)\includegraphics[width=0.4\textwidth,height=0.3\textwidth]{data/freqs_K_2_delta_0.19.eps}\\
(d)\includegraphics[width=0.4\textwidth,height=0.3\textwidth]{data/freqs_K_2_delta_2.eps}
(e)\includegraphics[width=0.4\textwidth,height=0.3\textwidth]{data/freqs_K_8_delta_0.19.eps}
\caption{(a) The $\Delta_2,K$ diagramm of dynamical regimes in the Kuramoto system with $N=100, \mu=0.1, \Delta_1=1, \xi=8$. (b) asynchronous state (c) two synchronous clusters (d) synchronous cluster and chimera state (e) global synchronous state.}
\label{fig::chimera_diag}
\end{figure}

\section{Conclusions}

In this paper we have studied the global synchronization mode of finite-demensional Kuramoto model. For this purpose we presented a normal form obtained by eliminating the uncertainty in the original Kuramoto model.  
We proposed a simple method of comparison which allowed to obtain explicit estimate of the phase synchronization range. This estimate is valid both for constant and time varying frequency mismatch. Detailed numerical simulations confirmed all theoretical results.
Cluster synchronization, chimera states are found.
We hope that this paper provide new income in the study of cooperative dynamics in oscillatory networks.


\section{Acknowledgements}
This research (Sections 1-3) was supported by the Russian Science Foundation (Project 14-12-00811) and by the grant N 14.B25.31.0008 between The Ministry of Education of Russian Federation and Lobachevsky State University of Nizhny Novgorod (Section 4).

\begin{thebibliography} {2}
%1
\bibitem{Winfree_1} A.T.~Winfree. Biolopgical rhythms and the behavior of coupled oscillators. J.Theoret. Biol. 16, 15-42, 1967.
%2
\bibitem{Kuramoto} Y.~Kuramoto, {\em Chemical Oscillations, Waves and Turbulence}, Springer Verlag, Berlin/D\"usseldorf, 1984.
%3
\bibitem{Kuramoto_1} Y.~Kuramoto, in Proc. of Int. Symp. on Mathematical Problems in Theoretical Physics, H.Araki, ed., Lecture Notes in Physics {\bf 39} (Springer, New York, 1975).
%4
\bibitem{Michaels} D.C.Michaels, E.P.Matyas, J.Jalife. Mechanisms of sinoatrial pacemaker synchronization: a new hypothesis. Circulation Research. 61(5), 704-714, 1987.
%5
\bibitem{Brown} E.Brown, P.Holmes, J.Moehlis. Globally coupled oscillator networks. In: E.Kaplan, J.E.Marsden, K.R.Sreenivasan (Eds.), Perspectives and Problems in Nonlinear Science: A Celebratory Volume in Honor of Larry Sirovich. Springer, 183-215, 2003.
%6
\bibitem{Kopell} N.Kopell, G.B.Ermentrout. Coupled oscillators and the design of central pattern generators. Mathematical Biosciences. 90, 87-109. 1988.
%7
\bibitem{Wiesenfeldt} K.Wiesenfeldt, P.Colet, S.Strogatz. Frequency locking in Josephson junction arrays: Connection with the Kuramoto model. Physical Review E, 57, 1563-1567, 1998.
%8
\bibitem{Neda} Z.Neda, E.Ravasz, T.Vicsek, Y.Brecht, A.-L.Barabasi. Physics of the rhythmic applause. Physical Review E, 61, 6987-6992, 2000.
%9
\bibitem{Strogatz} S.H.Strogatz, D.M.Abrams, A.McRobie, B.Eckhardt, E.Ott. Theoretical mechanics: Crowd synchrony on the Millenium Bridge. Nature. 438 (70640), 43-44, 2005.
%10
\bibitem{Kozyreff} G.Kozyrev, A.G.Vladimirov, P. Mandel. Global coupling with the time delay in an array  of semiconductor lasers. Physical Review Letters. 85(18), 3809-3812, 2000.
%11
\bibitem{York} R.A.York, R.C.Compton. Quasi-optical power combining using mutually synchronized oscillator arrays. IEEE Transactions on Automatic Control. 57(4), 920-935, 2012.
%12
\bibitem{Doerfler}   F. Dorfler and F. Bullo. Synchronization in Complex Networks of Phase Oscillators: A Survey. Automatica, 50(6), 1539-1564, 2014.
%13
\bibitem{Acebron}  J.A. Acebron, L.L.Bonilla, C.J.P. Vicente, F. Ritort, R.Spigler. The Kuramoto model: A simple paradigm for synchronization phenomena. Reviews of Modern Physics, 77(1), 137-185, 2005.
%14
\bibitem{Gupta} Sh.Gupta, A. Campa, S. Ruffo. Kuramoto model of synchronization: equilibrium and nonequilibrium aspects. Journal of statistical mechanics: Theory and Experiment, R08001, 2014.
%15
\bibitem{Aeyels} D.Aeyels, J.A.Rogge. Existence of partial entrainment and stability of phase locking behavior of coupled oscillators. Progress of Theoretical Physics. 112(6), 921-942, 2004.
%16
\bibitem{Mirollo} R.E.Mirollo, S.H.Strogatz. The spectrum of the partially locked state of the Kuramoto model of coupled oscillators. Physica D: Nonlinear Phenomena. 205(1-4), 249-266,2005.
%17
\bibitem{Verwoed} M.Verwoerd, O.Mason Global phase-lockingin finite population of phase-coupled oscillators. SIAM Journal on Applied Dynamical Systems. 7(1),134-160, 2008.
%18
\bibitem{Jadbabaie} A.Jadbabaie, N. Motee, N. Barahona. On the stability of the Kuramoto model of coupled oscillators. In: American Control Conference. Boston. MA. USA. 4296-4301,2004.



\end{thebibliography}

\end{document}
